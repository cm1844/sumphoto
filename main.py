import webapp2
import json
from upload import UploadHandler, UploadUrlHandler, SummariseHandler


class MainPage(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Hello, World!')

application = webapp2.WSGIApplication([
    # ('/', MainPage),  # makes no difference when app.yaml is set
    ('/upload/url', UploadUrlHandler),
    ('/upload/', UploadHandler),
    ('/summarise/', SummariseHandler)
], debug=True)
