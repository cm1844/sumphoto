/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: 'upload/'
//        , singleFileUploads: false
    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    if (window.location.hostname === 'blueimp.github.io') {
        // Demo settings:
        $('#fileupload').fileupload('option', {
            url: '//jquery-file-upload.appspot.com/',
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: '//jquery-file-upload.appspot.com/',
                type: 'HEAD'
            }).fail(function () {
                $('<div class="alert alert-danger"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .appendTo('#fileupload');
            });
        }
    } else {
        // Load existing files:
        $('#fileupload')
            .bind('fileuploadsubmit', function (e, data) {
                console.log('submitted!', data);
                $.ajax({
                    async: false,
                    url: 'upload/url',
                    dataType: "json",
                    success: function (result) {
                        data.url = result['uploadUrl'];
                    }
                });
            });
//            .bind('fileuploadstop', function (e) {
//
////                window.location.href = 'http://stackoverflow.com'
//            });

        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
        });

        $('#form-summarise').submit(function (event) {
            function render(result) {
                var source = $("#ajax-zip-url").html();
                var template = Handlebars.compile(source);
                $("#summarise").append(template(result));
            }

            event.preventDefault();
            var summariseUrl = 'summarise/';
            var $numPhotos = $('input[name=numPhotos]', this);
            var numPhotosString = $numPhotos.val().trim();
            console.log(numPhotosString);
            if (/^[0-9]+$/.test(numPhotosString)) {
                var numPhotos = Number(numPhotosString);
                if (numPhotos > 0) {
                    $.ajax({
                        url: summariseUrl,
                        type: 'POST',
                        data: {numPhotos: numPhotos},
                        dataType: 'json'
                    }).done(function (result) {
                        console.log('AJAX call to', summariseUrl, ' retrieved', result);
                        render(result);
                    });
                } else {
                    render({error: 'You need to specify at least 1 photo'})
                }
            } else {
                render({error: 'Numbers only!'});
            }
        });
    }

});
