import random
random.seed(0)

import numpy as np

class ImageFile(object):
    def __init__(self, name, size):
        self.name, self.size = name, size

    def __repr__(self):
        return str(self.size)
        return '%s(name=%s, size=%s)' % (self.__class__.__name__, repr(self.name), self.size)


names = ['Alice', 'Bob', 'Mike', 'Tom']


def get_clusters(image_files, num_clusters):
    def avg(ps):
        return float(sum([p.size for p in ps]))/len(ps)
    if num_clusters < 1:
        raise ValueError('Number of clusters has to be greater than 1')
    clusters = [[image_file] for image_file in image_files]  # initialise clusters to contain 1 element each
    while len(clusters) > num_clusters:
        differences = [abs(avg(clusters[i]) - avg(clusters[i+1])) for i in xrange(len(clusters)-1)]
        min_index, min_val = min(enumerate(differences), key=lambda x: x[1])

        extracted = clusters.pop(min_index+1)
        clusters[min_index].extend(extracted)

    return clusters


def get_lucky_clusters(image_files):
    def avg(ps):
        return float(sum([p.size for p in ps]))/len(ps)

    clusters = [[image_file] for image_file in image_files]  # initialise clusters to contain 1 element each
    while True:
        differences = np.array([abs(avg(clusters[i]) - avg(clusters[i+1])) for i in xrange(len(clusters)-1)])
        min_val = np.nanmin(differences)
        min_index = np.nanargmin(differences)
        print 'Here0', differences, min_index, min_val
        # do not merge while diff > 0.2 * max size and there are any non-NaN's (i.e. there are numbers)
        while min_val > (0.2 * max(avg(clusters[min_index]), avg(clusters[min_index+1]))):
            print 'Here1', differences, min_index, min_val
            differences[min_index] = float('NaN')
            if all([np.isnan(difference) for difference in differences]):
                break
            min_val = np.nanmin(differences)
            min_index = np.nanargmin(differences)

        print 'Here2', differences, min_index, min_val
        if all([np.isnan(difference) for difference in differences]):
            break

        extracted = clusters.pop(min_index+1)
        clusters[min_index].extend(extracted)  # merge the extracted cluster into another cluster
        print 'clusters', clusters

    return clusters

entities = []


sizes = [1, 4, 6, 7, 9, 15, 16, 17, 22]

for i in xrange(len(sizes)):
    entities.append(ImageFile('%s %s.jpeg' % (names[random.randint(0, len(names) - 1)], i), sizes[i]))

print entities

print 'clusters end %s' % get_lucky_clusters(entities)
