val = '1'
try:
	numPhotos = int(val)
	if numPhotos > 0:
		print 'fine'
	else:
		print 'else'
		raise ValueError
except ValueError:
	print 'not fine'
finally:
	print 'finally'


exit(1)


def get_clusters(data, comparison_f):
	threshold = 3
	clusters = {}

	if data:
		cluster_num = 0
		clusters[cluster_num] = [data[0]]

		for i in range(len(data) - 1):
			if abs(comparison_f(data[i], data[i+1])) <= threshold:
				clusters[cluster_num].append(data[i+1])
			else:
				cluster_num += 1
				clusters[cluster_num] = [data[i+1]]
	print threshold
	return clusters


class Entity(object):
	def __init__(self, i):
		self.datetime_original = i
		self.name = 'Name %d' % i

	def __repr__(self):
		return 'Name: %s, datetime: %d' % (self.name, self.datetime_original)

numbers = [1, 2, 3, 6, 9, 10, 134, 454, 700]

entities = [Entity(i) for i in numbers]
print entities

# print data

clusters = get_clusters(entities, lambda x, y: x.datetime_original - y.datetime_original)


for c in clusters:
	print clusters[c][-1]


