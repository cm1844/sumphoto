# -*- coding: utf-8 -*-
#
# jQuery File Upload Plugin GAE Python Example 2.2.0
# https://github.com/blueimp/jQuery-File-Upload
#
# Copyright 2011, Sebastian Tschan
# https://blueimp.net
#
# Licensed under the MIT license:
# http://www.opensource.org/licenses/MIT
#

from __future__ import with_statement
from google.appengine.api import images, app_identity
from google.appengine.ext import blobstore, deferred, ndb
from google.appengine.ext.webapp import blobstore_handlers
import cloudstorage as gcs
import json
import re
import urllib
import webapp2
import os
import logging
import zipfile
import datetime
import gc
import numpy as np

WEBSITE = '/'
UPLOAD_ROUTE = '/upload/'  # note: end the route with '/'
MIN_FILE_SIZE = 1  # bytes
MAX_FILE_SIZE = 15000000  # bytes
IMAGE_TYPES = re.compile('image/(gif|p?jpeg|(x-)?png)')
ACCEPT_FILE_TYPES = IMAGE_TYPES
THUMBNAIL_MODIFICATOR = '=s80'  # max width / height
EXPIRATION_TIME = 300  # seconds
BUCKET_NAME = app_identity.get_default_gcs_bucket_name()
BUCKET = '/' + BUCKET_NAME
UPLOAD_FOLDER = '/upload'  # ensure starts with a slash!
DUPLICATE_FILENAME_REGEX = re.compile('(.+) \((\d+)\)$')


def cleanup(entity_keys):
    ndb.delete_multi(entity_keys)


class ImageFile(ndb.Model):
    filename = ndb.StringProperty(required=True)
    gcs_filename = ndb.StringProperty(required=True)
    blob_key = ndb.StringProperty(required=True)
    serving_url = ndb.StringProperty(required=True)
    # datetime_original = ndb.DateTimeProperty(required=True)
    content_type = ndb.StringProperty(required=True, indexed=False)
    size = ndb.IntegerProperty(required=True, indexed=False)

    @classmethod
    def new(cls, filename, gs_filename, blob_key, content_type, size, using_secure_url):
        return cls(filename=filename, gcs_filename=gs_filename[3:], blob_key=blob_key,
                   serving_url=images.get_serving_url(blob_key, secure_url=using_secure_url),
                   content_type=content_type, size=size)

    def read_blob(self):  # TODO investigate using Blobstore API to read this blob
        """ read binary blob from google cloud storage """
        try:
            with gcs.open(self.gcs_filename) as f:
                return f.read()
            # return blobstore.BlobReader(self.blob_key).read()
        except gcs.NotFoundError, e:
            logging.warning("GCS file '%s' NOT FOUND : %s" % (self.gcs_filename, e))
            return None

    def delete_blob(self):
        logging.info('delete_blob called')
        images.delete_serving_url(self.blob_key)
        blobstore.delete(self.blob_key)

    @classmethod
    def _pre_delete_hook(cls, key):
        logging.info('pre_delete_hook called on key %s' % key)
        image_file = key.get()
        if image_file:
            image_file.delete_blob()
        else:
            logging.warning("Entity with key '%s' not found" % key)


class UploadHandler(blobstore_handlers.BlobstoreUploadHandler):

    def validate(self, file):
        if file['size'] < MIN_FILE_SIZE:
            file['error'] = 'File is too small'
        elif file['size'] > MAX_FILE_SIZE:
            file['error'] = 'File is too big'
        elif not ACCEPT_FILE_TYPES.match(file['type']):
            file['error'] = 'Filetype not allowed'
        else:
            return True
        return False

    def get_datetime_original(self, gs_filename):
        image = images.Image(filename=gs_filename)
        image.rotate(0)
        image.execute_transforms(parse_source_metadata=True)
        metadata = image.get_original_metadata()
        datetime_original_str = metadata.get('DateTimeOriginal')
        if datetime_original_str:
            return datetime.datetime.utcfromtimestamp(int(datetime_original_str))
        else:
            return None

    # def get_file_size(self, file):
    #     file.seek(0, 2)  # Seek to the end of the file
    #     size = file.tell()  # Get the position of EOF
    #     file.seek(0)  # Reset the file position to the beginning
    #     return size

    def handle_upload(self):
        results = []
        entity_keys = []
        blob_keys_to_delete = []

        for file_info in self.get_file_infos():
            logging.info('name=%s, gs_object_name: %s, size=%s, type=%s, creation=%s' % (file_info.filename, file_info.gs_object_name, file_info.size, file_info.content_type, file_info.creation))
            blob_key = blobstore.create_gs_key(file_info.gs_object_name)
            result = {}
            result['name'] = re.sub(
                r'^.*\\',
                '',
                file_info.filename
            )
            result['type'] = file_info.content_type
            result['size'] = file_info.size
            if self.validate(result):
                datetime_original = datetime.datetime.utcnow()  # self.get_datetime_original(file_info.gs_object_name) TODO fix
                if datetime_original:
                    image_file = ImageFile.new(result['name'], file_info.gs_object_name, blob_key, file_info.content_type, file_info.size, self.request.host_url.startswith('https'))
                    entity_key = image_file.put()
                    entity_keys.append(entity_key)

                    result['deleteType'] = 'DELETE'
                    result['deleteUrl'] = self.request.host_url +\
                        UPLOAD_ROUTE + '?key=' + entity_key.urlsafe()
                    result['url'] = image_file.serving_url
                    result['thumbnailUrl'] = result['url'] +\
                        THUMBNAIL_MODIFICATOR
                else:
                    result['error'] = 'Image does not have DateTimeOriginal'
                    blob_keys_to_delete.append(blob_key)
            else:
                blob_keys_to_delete.append(blob_key)
            results.append(result)
        if blob_keys_to_delete:
            blobstore.delete(blob_keys_to_delete)
        '''deferred.defer(  # TODO put back in
            cleanup,
            entity_keys,
            _countdown=EXPIRATION_TIME
        )'''
        return results

    def options(self):
        pass

    def head(self):
        pass

    def get(self):
        result = {'files': []}
        for image_file in ImageFile.query().order(ImageFile.filename):
            result['files'].append({
                'name': image_file.filename,
                'size': image_file.size,
                'type': image_file.content_type,
                'deleteType': 'DELETE',
                'deleteUrl': self.request.host_url + UPLOAD_ROUTE + '?key=' + image_file.key.urlsafe(),
                'url': image_file.serving_url,
                'thumbnailUrl': image_file.serving_url + THUMBNAIL_MODIFICATOR,
            })

        s = json.dumps(result, separators=(',', ':'))
        if 'application/json' in self.request.headers.get('Accept'):
                self.response.headers['Content-Type'] = 'application/json'
        self.response.write(s)

    def post(self):
        if (self.request.get('_method') == 'DELETE'):
            return self.delete()
        result = {'files': self.handle_upload()}
        s = json.dumps(result, separators=(',', ':'))
        redirect = self.request.get('redirect')
        if redirect:
            return self.redirect(str(
                redirect.replace('%s', urllib.quote(s, ''), 1)
            ))
        if 'application/json' in self.request.headers.get('Accept'):
            self.response.headers['Content-Type'] = 'application/json'
        self.response.write(s)

    def delete(self):
        key_str = self.request.get('key')
        deleted = False
        try:
            entity_key = ndb.Key(urlsafe=key_str)
            entity_key.delete()
            deleted = True
        except Exception as e:
            logging.exception("Failed to delete using key '%s' : %s" % (key_str, e))
        s = json.dumps({key_str: deleted}, separators=(',', ':'))
        if 'application/json' in self.request.headers.get('Accept'):
            self.response.headers['Content-Type'] = 'application/json'
        self.response.write(s)


class UploadUrlHandler(webapp2.RequestHandler):
    def get(self):
        upload_url = blobstore.create_upload_url(UPLOAD_ROUTE, max_bytes_per_blob=MAX_FILE_SIZE, gs_bucket_name=BUCKET_NAME + UPLOAD_FOLDER)
        s = json.dumps({'uploadUrl': upload_url}, separators=(',', ':'))
        if 'application/json' in self.request.headers.get('Accept'):
            self.response.headers['Content-Type'] = 'application/json'
        self.response.write(s)


# app = webapp2.WSGIApplication(
#     [
#         (UPLOAD_ROUTE, UploadHandler),
#         (UPLOAD_ROUTE + '([^/]+)/([^/]+)', DownloadHandler)
#     ],
#     debug=True
# )


class SummariseHandler(webapp2.RequestHandler):
    def post(self):
        try:
            num_photos = int(self.request.get('numPhotos'))
            if num_photos > 0:
                data = get_images_to_archive(num_photos)
                archive_path = create_archive('archive.zip', data)
                if os.environ['SERVER_SOFTWARE'].startswith('Development'):
                    # GCS url: this SDK feature has not been documented yet !!!
                    zip_url = '/_ah/gcs%s' % archive_path
                else:
                    zip_url = '//storage.cloud.google.com%s' % archive_path
                result = {'zipUrl': zip_url}
            else:
                raise ValueError
        except ValueError:
            logging.warning("SummariseHandler received invalid numPhotos = '%s'" % self.request.get('numPhotos'))
            result = {'error': 'Invalid number of photos specified'}
        finally:
            s = json.dumps(result, separators=(',', ':'))
            if 'application/json' in self.request.headers.get('Accept'):
                    self.response.headers['Content-Type'] = 'application/json'
            self.response.write(s)

def get_filename(gcs_dir, orig_filename):
    raise NotImplementedError
    def _get_filename(filename, first_call=False):
        try:
            gcs.stat(gcs_dir + filename)  # TODO deal with race condition
        except gcs.NotFoundError:  # file doesn't exist, i.e. we can use this name
            return filename
        else:  # requested filename already in use
            filename, ext = filename.rsplit('.', 1)
            if first_call:  # if first call, then append ' (1)' and recurse
                return _get_filename('%s (1).%s' % (filename, ext))
            else:  # function called before, so filename must match the format of 'name (number)'
                filename, number_str = DUPLICATE_FILENAME_REGEX.match(filename).groups()
                return _get_filename('%s (%d).%s' % (filename, int(number_str) + 1, ext))

    return _get_filename(orig_filename, first_call=True)


def get_images_to_archive(num_photos):
    def avg(ps):
            return float(sum([p.size for p in ps]))/len(ps)

    def get_clusters(image_files, num_clusters):
        if num_clusters < 1:
            raise ValueError('Number of clusters has to be greater than 1')

        clusters = [[image_file] for image_file in image_files]  # initialise clusters to contain 1 element each
        while len(clusters) > num_clusters:
            differences = [abs(avg(clusters[i]) - avg(clusters[i+1])) for i in xrange(len(clusters)-1)]
            min_index, min_val = min(enumerate(differences), key=lambda x: x[1])
            extracted = clusters.pop(min_index+1)
            clusters[min_index].extend(extracted)  # merge the extracted cluster into another cluster

        return clusters

    def get_lucky_clusters(image_files):
        clusters = [[image_file] for image_file in image_files]  # initialise clusters to contain 1 element each
        while True:
            differences = np.array([abs(clusters[i][-1].size - clusters[i+1][0].size) for i in xrange(len(clusters)-1)], dtype=np.float)
            min_val = np.nanmin(differences)
            min_index = np.nanargmin(differences)
            # do not merge while diff > 0.2 * max size and there are any non-NaN's (i.e. there are numbers)
            while min_val > (0.25 * min(clusters[min_index][-1].size, clusters[min_index+1][0].size)):
                differences[min_index] = np.nan
                if all([np.isnan(difference) for difference in differences]):
                    break
                min_val = np.nanmin(differences)
                min_index = np.nanargmin(differences)

            if all([np.isnan(difference) for difference in differences]):
                break

            extracted = clusters.pop(min_index+1)
            clusters[min_index].extend(extracted)  # merge the extracted cluster into another cluster

        return clusters

    entities = ImageFile.query().order(ImageFile.filename).fetch()
    logging.info('Summarising %s photos' % len(entities))
    clusters = get_lucky_clusters(entities)
    '''
    results = []
    for cluster in clusters:
        print 'Cluster =', [i.filename for i in cluster]
        image_file = cluster[-1]  # select the last image in the cluster
        print 'image =', image_file.filename
        results.append(image_file)
    return results
    '''
    return [cluster[-1] for cluster in clusters]


def create_archive(archive_name, image_files, folder=''):
    gc.set_threshold(2,1,1)
    gcs_dir = BUCKET + folder + '/'
    archive_filename = gcs_dir + archive_name
    with gcs.open(archive_filename, 'w', content_type='multipart/x-zip',
                  options={'x-goog-acl': 'public-read', 'Cache-Control': 'private, max-age=0, no-transform'}) as f:
        with zipfile.ZipFile(f, 'w') as zf:
            for image_file in image_files:
                filename = '{1} {0}{2}'.format(image_file.gcs_filename.rsplit('/', 1)[1], *os.path.splitext(image_file.filename))
                blob = image_file.read_blob()
                if blob is not None:
                    logging.info('Archiving %s' % filename)
                    zf.writestr(filename.encode('utf-8'), blob)
                else:
                    logging.warning("Blob '%s' was none!" % filename)
                gc.collect()
    return archive_filename
